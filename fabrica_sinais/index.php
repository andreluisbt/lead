<!DOCTYPE html>
<html>
	<head>
		<?php include_once '../components/head.php';?>
		
        <title>Fábrica de sinais</title>

        <script type="text/javascript" src="../js/signs-factory.js"></script>
	</head>

	<body id="signsFactoryHome" class="signs-factory">
		<div id="wrapPage">
			
			<header>
				<?php include_once '../components/fabrica_sinais/header.php';?>
			</header>
            
            <div class="container">
                <aside class="col-md-3">
                    <img src="<?php echo $SYSTEM->rooturl; ?>/imgs/logo-dell-blue.png" class="logo" alt="Accessible learning">
                    
                    <h6>
                       Meu painel 
                    </h6>
                    <ul>
                        <li>
                            <a href="#" class="btn btn-primary">Meu perfil</a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-primary">Minhas configurações</a>
                        </li>
                    </ul>
                    
                    <h6>
                       Busca 
                    </h6>
                    <form class="search">
                        <div class="form-group">
                            <input type="text" id="subject" name="subject" class="form-control">
                            <button type="submit" class="btn btn-primary">OK</button>
                        </div>
                    </form>
                    <div class="clearfix"></div>
                    
                    <h6>
                       Categorias 
                    </h6>
                    <ul>
                        <li>
                            <a href="#" class="btn btn-primary">Java</a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-primary">Mobile</a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-primary">Oracle</a>
                        </li>
                    </ul>
                    <!--
                    <nav id="navigationBar" class="navbar">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigationBar .navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        
                    </nav>
                    -->  
                </aside>
                <nav class="col-md-9">
                    <ul class="tabs">
                        <li>
                            <a href="#">Sinal</a>
                        </li>
                        <li>
                            <a href="#" class="active">Discussão</a>
                        </li>
                        <li>
                            <a href="#">Sobre</a>
                        </li>
                    </ul>
                    <ul class="user-info hidden-xs">
                        <li>
                            <a href="#">Logado como Eugine</a>
                        </li>
                        <li>
                            <a href="#">Sair</a>
                        </li>
                    </ul>
                </nav>
                <section id="main" class="col-md-9">
                    <h5 class="page-title">
                        Herança
                    </h5>
                    <div class="wrap-video col-md-6 col-md-offset-3">
                        <video class="col-md-12" src="http://www.w3schools.com/tags/movie.mp4" controls></video>
                    </div>
                    <div class="clearfix"></div>
                    <div class="description">
                        <h6>Descrição</h6>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. In fringilla justo vel pharetra dignissim. Sed tincidunt tellus sit amet sapien semper interdum. Maecenas lectus orci, dictum ut tellus nec, bibendum luctus neque. Proin at tellus id urna faucibus ultricies. Suspendisse ut odio commodo, rhoncus magna sit amet, ullamcorper nulla.
                        <a href="#">Mais</a>
                    </div>
                    
                    <h5 class="page-title">
                        Sinais sugeridos
                    </h5>

                    <h5 class="signs-group-title">Mais votados</h5>
                    <div class="signs-mask">
                        <ul class="controls">
                            <li class="left">
                                <a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a>
                            </li>
                            <li class="right">
                                <a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>
                            </li>
                        </ul>
                        <ul class="signs-list">
                            <li>
                                <a href="#">
                                    <img src="<?php echo $SYSTEM->rooturl; ?>/imgs/sign.png">
                                </a>
                                <a href="#" class="by">por Eugine</a>
                                <div class="date">18/09/15</div>
                                <span class="glyphicon glyphicon-thumbs-up"></span> 20
                            </li>
                            <li>
                                <a href="#">
                                    <img src="<?php echo $SYSTEM->rooturl; ?>/imgs/sign.png">
                                </a>
                                <a href="#" class="by">por Eugine</a>
                                <div class="date">18/09/15</div>
                                <span class="glyphicon glyphicon-thumbs-up"></span> 20
                            </li>
                            <li>
                                <a href="#">
                                    <img src="<?php echo $SYSTEM->rooturl; ?>/imgs/sign.png">
                                </a>
                                <a href="#" class="by">por Eugine</a>
                                <div class="date">18/09/15</div>
                                <span class="glyphicon glyphicon-thumbs-up"></span> 20
                            </li>
                            <li>
                                <a href="#">
                                    <img src="<?php echo $SYSTEM->rooturl; ?>/imgs/sign.png">
                                </a>
                                <a href="#" class="by">por Eugine</a>
                                <div class="date">18/09/15</div>
                                <span class="glyphicon glyphicon-thumbs-up"></span> 20
                            </li>
                            <li>
                                <a href="#">
                                    <img src="<?php echo $SYSTEM->rooturl; ?>/imgs/sign.png">
                                </a>
                                <a href="#" class="by">por Eugine</a>
                                <div class="date">18/09/15</div>
                                <span class="glyphicon glyphicon-thumbs-up"></span> 20
                            </li>
                            <li>
                                <a href="#">
                                    <img src="<?php echo $SYSTEM->rooturl; ?>/imgs/sign.png">
                                </a>
                                <a href="#" class="by">por Eugine</a>
                                <div class="date">18/09/15</div>
                                <span class="glyphicon glyphicon-thumbs-up"></span> 20
                            </li>
                            <li>
                                <a href="#">
                                    <img src="<?php echo $SYSTEM->rooturl; ?>/imgs/sign.png">
                                </a>
                                <a href="#" class="by">por Eugine</a>
                                <div class="date">18/09/15</div>
                                <span class="glyphicon glyphicon-thumbs-up"></span> 20
                            </li>
                            <li>
                                <a href="#">
                                    <img src="<?php echo $SYSTEM->rooturl; ?>/imgs/sign.png">
                                </a>
                                <a href="#" class="by">por Eugine</a>
                                <div class="date">18/09/15</div>
                                <span class="glyphicon glyphicon-thumbs-up"></span> 20
                            </li>
                        </ul>
                    </div>


                    <h5 class="signs-group-title">Mais recentes</h5>
                    <div class="signs-mask">
                        <ul class="controls">
                            <li class="left">
                                <a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a>
                            </li>
                            <li class="right">
                                <a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a>
                            </li>
                        </ul>
                        <ul class="signs-list">
                            <li>
                                <a href="#">
                                    <img src="<?php echo $SYSTEM->rooturl; ?>/imgs/sign.png">
                                </a>
                                <a href="#" class="by">por Eugine</a>
                                <div class="date">18/09/15</div>
                                <span class="glyphicon glyphicon-thumbs-up"></span> 20
                            </li>
                            <li>
                                <a href="#">
                                    <img src="<?php echo $SYSTEM->rooturl; ?>/imgs/sign.png">
                                </a>
                                <a href="#" class="by">por Eugine</a>
                                <div class="date">18/09/15</div>
                                <span class="glyphicon glyphicon-thumbs-up"></span> 20
                            </li>
                            <li>
                                <a href="#">
                                    <img src="<?php echo $SYSTEM->rooturl; ?>/imgs/sign.png">
                                </a>
                                <a href="#" class="by">por Eugine</a>
                                <div class="date">18/09/15</div>
                                <span class="glyphicon glyphicon-thumbs-up"></span> 20
                            </li>
                            <li>
                                <a href="#">
                                    <img src="<?php echo $SYSTEM->rooturl; ?>/imgs/sign.png">
                                </a>
                                <a href="#" class="by">por Eugine</a>
                                <div class="date">18/09/15</div>
                                <span class="glyphicon glyphicon-thumbs-up"></span> 20
                            </li>
                            <li>
                                <a href="#">
                                    <img src="<?php echo $SYSTEM->rooturl; ?>/imgs/sign.png">
                                </a>
                                <a href="#" class="by">por Eugine</a>
                                <div class="date">18/09/15</div>
                                <span class="glyphicon glyphicon-thumbs-up"></span> 20
                            </li>
                            <li>
                                <a href="#">
                                    <img src="<?php echo $SYSTEM->rooturl; ?>/imgs/sign.png">
                                </a>
                                <a href="#" class="by">por Eugine</a>
                                <div class="date">18/09/15</div>
                                <span class="glyphicon glyphicon-thumbs-up"></span> 20
                            </li>
                            <li>
                                <a href="#">
                                    <img src="<?php echo $SYSTEM->rooturl; ?>/imgs/sign.png">
                                </a>
                                <a href="#" class="by">por Eugine</a>
                                <div class="date">18/09/15</div>
                                <span class="glyphicon glyphicon-thumbs-up"></span> 20
                            </li>
                            <li>
                                <a href="#">
                                    <img src="<?php echo $SYSTEM->rooturl; ?>/imgs/sign.png">
                                </a>
                                <a href="#" class="by">por Eugine</a>
                                <div class="date">18/09/15</div>
                                <span class="glyphicon glyphicon-thumbs-up"></span> 20
                            </li>
                        </ul>
                    </div>
                    <br/><br/>
                    <a href="#" class="btn btn-block btn-primary"><span class="glyphicon glyphicon-plus-sign"></span>&nbsp;&nbsp;SUGERIR SINAL</a>
                    <br/>

                    <div class="comments">
                        <h5 class="page-title">
                            Comentários
                        </h5>
                        <div class="text-center">
                            <ul class="pagination">
                                <li class="active">
                                    <a href="#">1</a>
                                </li>
                                <li>
                                    <a href="#">2</a>
                                </li>
                                <li>
                                    <a href="#">3</a>
                                </li>
                                <li>
                                    <a href="#">4</a>
                                </li>
                            </ul>
                        </div>
                        <select class="num-show">
                            <option>Mostrar 5</option>
                            <option>Mostrar 5</option>
                            <option>Mostrar 5</option>
                        </select>

                        <ul class="comments-list">
                            <li>
                                <div>
                                    Meu sinal é muito mais show de bola ;p
                                </div>
                                <div class="by">Enviado po Daniel</div>
                            </li>
                            <li>
                                <div>
                                    <video src="http://www.w3schools.com/tags/movie.mp4" controls></video>
                                </div>
                                <div class="by">Enviado po Daniel</div>
                            </li>
                            <li>
                                <div>
                                    Meu sinal é muito mais show de bola ;p
                                </div>
                                <div class="by">Enviado po Daniel</div>
                            </li>
                            <li>
                                <div>
                                    <video src="http://www.w3schools.com/tags/movie.mp4" controls></video>
                                </div>
                                <div class="by">Enviado po Daniel</div>
                            </li>
                            <li>
                                <div>
                                    Meu sinal é muito mais show de bola ;p
                                </div>
                                <div class="by">Enviado po Daniel</div>
                            </li>
                            <li>
                                <div>
                                    <video src="http://www.w3schools.com/tags/movie.mp4" controls></video>
                                </div>
                                <div class="by">Enviado po Daniel</div>
                            </li>
                            <li>
                                <div>
                                    Meu sinal é muito mais show de bola ;p
                                </div>
                                <div class="by">Enviado po Daniel</div>
                            </li>
                            <li>
                                <div>
                                    <video src="http://www.w3schools.com/tags/movie.mp4" controls></video>
                                </div>
                                <div class="by">Enviado po Daniel</div>
                            </li>
                        </ul>

                        <div class="text-center">
                            <ul class="pagination">
                                <li class="active">
                                    <a href="#">1</a>
                                </li>
                                <li>
                                    <a href="#">2</a>
                                </li>
                                <li>
                                    <a href="#">3</a>
                                </li>
                                <li>
                                    <a href="#">4</a>
                                </li>
                            </ul>
                        </div>
                        <select class="num-show">
                            <option>Mostrar 5</option>
                            <option>Mostrar 5</option>
                            <option>Mostrar 5</option>
                        </select>
                        
                        <div class="form-comment-wrap">
                            <div class="col-sm-2 col-md-2">
                                <button id="btnAddVideo" class="col-xs-6 col-sm-12 col-md-12">
                                    <span class="glyphicon glyphicon-facetime-video"></span>
                                </button>
                                <button id="btnAddFile" class="col-xs-6 col-sm-12 col-md-12">
                                    <span class="glyphicon glyphicon-file"></span>
                                </button>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-sm-10 col-md-10">
                                <form>
                                    <div class="form-group">
                                        <textarea name="comment" class="form-control summernote" placeholder="Comentário"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Enviar</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                    <div class="clearfix"></div>
                </section>
            </div>
            
            
            <!--
	            <h4 class="page-title">CURSOS EM ANDAMENTO</h4>
	            
	            <div class="couses-in-progress">
	                <a href="lessons.php">
    	                <div class="pie-chart" data-percent="73">
    	                    <span>Java básico <br/> 73%</span>
    	                </div>
	                </a>
	                <a href="lessons.php">
    	                <div class="pie-chart" data-percent="57">
    	                    <span>Java Intermediário <br/> 57%</span>
    	                </div>
    	            </a>
	            </div>
	            
	            <div id="calendar" class="calendar col-md-6">
	                <div class="all-months col-md-12 active">
    	                <div class="year-bar">
    	                    <a href="#" class="btn prev">
    	                        <i class="fa fa-chevron-left"></i>
    	                    </a>
    	                    <div class="year">2015</div>
    	                    <a href="#" class="btn next">
    	                        <i class="fa fa-chevron-right"></i>
    	                    </a>
    	                </div>
    	                
    	                <div class="courses-list">
    	                    <select>
    	                        <option value="0">Selecione um curso</option>
    	                        <option value="1">Java Básico</option>
    	                        <option value="2">Java Intermediário</option>
    	                    </select>
    	                </div>
    	                
    	                <div class="months">
    	                    <ul>
                                <li class="col-xs-6 col-sm-4 col-md-3">
                                    <div>
                                        <div>JAN</div>
                                    </div>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3">
                                    <div>
                                        <div>JAN</div>
                                    </div>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3">
                                    <div>
                                        <div>JAN</div>
                                    </div>
                                </li>
    	                        <li class="col-xs-6 col-sm-4 col-md-3">
                                    <a href="#">
                                        <div>JAN</div>
                                        <span>5</span>
                                        COMPROMISSOS
                                    </a>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3">
                                    <div>
                                        <div>JAN</div>
                                    </div>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3">
                                    <div>
                                        <div>JAN</div>
                                    </div>
                                </li>
    	                        <li class="col-xs-6 col-sm-4 col-md-3">
    	                            <a href="#">
    	                                <div>JAN</div>
    	                                <span>5</span>
    	                                COMPROMISSOS
    	                            </a>
    	                        </li>
                                <li class="col-xs-6 col-sm-4 col-md-3">
                                    <div>
                                        <div>JAN</div>
                                    </div>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3">
                                    <div>
                                        <div>JAN</div>
                                    </div>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3">
                                    <div>
                                        <div>JAN</div>
                                    </div>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3">
                                    <div>
                                        <div>JAN</div>
                                    </div>
                                </li>
                                <li class="col-xs-6 col-sm-4 col-md-3">
                                    <div>
                                        <div>JAN</div>
                                    </div>
                                </li>
    	                    </ul>
	                   </div>
	                </div>
	                <div class="all-tasks col-md-12">
						<div class="panel panel-primary">
							<div class="panel-heading">
								COMPROMISSOS
							</div>
							<div class="panel-body">
                                <a href="#" class="back">
                                    <span>
                                        <i class="fa fa-angle-left"></i>
                                    </span>
                                    VOLTAR
                                </a>
                                
                                <div class="scroll">
                                    <div class="days">
                                        <div class="week">
                                            <ul>
                                                <li class="name sunday">DOM</li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <span class="flag"></span>
                                                        23
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                            </ul>
                                            
                                        </div>
                                        <div class="week">
                                            <ul>
                                                <li class="name">SEG</li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                                <li class="today">
                                                    <a href="#">23</a>
                                                </li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="week">
                                            <ul>
                                                <li class="name">TER</li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="week">
                                            <ul>
                                                <li class="name">QUA</li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="week">
                                            <ul>
                                                <li class="name">QUI</li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="week">
                                            <ul>
                                                <li class="name">SEX</li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                            </ul>
                                            
                                        </div>
                                        <div class="week">
                                            <ul>
                                                <li class="name saturday">SAB</li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                                <li>
                                                    <a href="#">23</a>
                                                </li>
                                            </ul>
                                            
                                        </div>
                                    </div>
                                    
                                    <div class="events">
                                        <div class="event">
                                            <div class="title">Java Android</div>
                                            <div class="date">07/06</div>
                                            <div class="description">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                            </div>
                                        </div>
                                        <div class="event">
                                            <div class="title">Java Android</div>
                                            <div class="date">07/06</div>
                                            <div class="description">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                            </div>
                                        </div>
                                        <div class="event">
                                            <div class="title">Java Android</div>
                                            <div class="date">07/06</div>
                                            <div class="description">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
	            </div>
            
	            <div id="inbox" class="col-md-6">
	                <table class="table table-hover">
	    				<thead>
	    					<tr>
	    						<th>DE</th>
	    						<th>ASSUNTO</th>
	    						<th>DATA</th>
	    					</tr>
	    				</thead>
	    				<tbody>
	    					<tr>
	    						<td><img src="imgs/user.jpg"></td>
	    						<td>Dúvidas sobre a aula 03 de JAVA</td>
	    						<td>04/05/2015</td>
	    					</tr>
	    					<tr>
	                            <td><img src="imgs/user.jpg"></td>
	                            <td>Dúvidas sobre a aula 03 de JAVA</td>
	                            <td>04/05/2015</td>
	                        </tr>
	                        <tr>
	                            <td><img src="imgs/user.jpg"></td>
	                            <td>Dúvidas sobre a aula 03 de JAVA</td>
	                            <td>04/05/2015</td>
	                        </tr>
	                        <tr>
	                            <td><img src="imgs/user.jpg"></td>
	                            <td>Dúvidas sobre a aula 03 de JAVA</td>
	                            <td>04/05/2015</td>
	                        </tr>
	                        <tr>
	                            <td><img src="imgs/user.jpg"></td>
	                            <td>Dúvidas sobre a aula 03 de JAVA</td>
	                            <td>04/05/2015</td>
	                        </tr>
	                        <tr>
	                            <td><img src="imgs/user.jpg"></td>
	                            <td>Dúvidas sobre a aula 03 de JAVA</td>
	                            <td>04/05/2015</td>
	                        </tr>
	    				</tbody>
	    			</table>
	    			
	    			<button type="button" class="btn btn-danger btn-block">VER MAIS</button>
	            </div>
            
	            <div id="videoConference" class="col-md-6">
	                
	                <div class="panel panel-success lead-panel">
						<div class="panel-heading">
							VÍDEO CONFERÊNCIA
						</div>
						<div class="panel-body">
							Nenhuma vídeo conferência disponivel no momento
						</div>
					</div>
					
					<ul>
					    <li>
					        <a href="#">Turma de Ruby - Assunto da vídeo conferência</a>
					    </li>
					    <li>
	                        <a href="#">Turma de Java Básico - Assunto da vídeo conferência</a>
	                    </li>
	                    <li>
	                        <a href="#">Turma de Ruby - Assunto da vídeo conferência</a>
	                    </li>
	                    <li>
	                        <a href="#">Turma de Java Básico - Assunto da vídeo conferência</a>
	                    </li>
	                    <li>
	                        <a href="#">Turma de Ruby - Assunto da vídeo conferência</a>
	                    </li>
					</ul>
	                
	                <button type="button" class="btn btn-success btn-block">LISTA COMPLETA</button>
	            </div>
            
	            <div id="grades" class="col-md-6">
	                
	                <div class="panel panel-purple lead-panel">
	                    <div class="panel-heading">
	                        NOTAS
	                    </div>
	                    <div class="panel-body">
	                        
	                        <div class="courses-list">
	                            <select>
			                        <option value="0">Selecione um curso</option>
			                        <option value="1">Java Básico</option>
			                        <option value="2">Java Intermediário</option>
			                    </select>
	                        </div>
	                        
	                        <div class="grade-description">
		                        <ul>
		                            <li class="col-sm-4 col-md-4">
		                                <span>8.6</span>
		                                MÉDIA DO CURSO
		                            </li>
		                            <li class="col-sm-4 col-md-4">
		                                <span>8.6</span>
		                                MÉDIA DO MÓDULO
		                            </li>
		                            <li class="col-sm-4 col-md-4">
		                                <span>8.6</span>
		                                NOTA DA AULA
		                            </li>
		                        </ul>
							</div>
	                    </div>
	                </div>
	                <button type="button" class="btn btn-purple btn-block">CONSULTAR</button>
	            </div>
	           
            </section>
            
            <?php include_once 'components/products.php';?>
		  -->
          <!--
			<footer>
				<?php include_once 'components/footer.php';?>
			</footer>
		-->
		</div>		

	</body>
</html>