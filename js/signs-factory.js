$("document").ready(function(){
	
	$('.signs-mask .signs-list').each(function(e){
		var countItems = $(this).find('li').length;
		$(this).css('width', (countItems*130)+110);
	});

	$('.signs-mask .controls').on('click', '.right a', function(e){
		e.preventDefault();
		signsScroll($(this), true);
	});

	$('.signs-mask .controls').on('click', '.left a', function(e){
		e.preventDefault();
		signsScroll($(this), false);
	});
	
	/*
	* leftRight - TRUE to margin-left down and FALSE to margin-left up 
	*/
	signsScroll = function($ele, leftRight){

		$signsMask = $ele.parents('.signs-mask');
		$signsList = $signsMask.find('.signs-list');

		signsMaskWidth = $signsMask.width();
		signsListWidth = $signsList.width();
		signsListMargin = parseInt($signsList.css('marginLeft').replace('px', ''));
			
		if(leftRight){
			if((signsListWidth+signsListMargin) > signsMaskWidth){
				newMarginLeft = signsListMargin-170;
				$signsList.css('marginLeft', newMarginLeft);
			}
		}else{
			if(signsListMargin < 0){
				newMarginLeft = signsListMargin+170;
				$signsList.css('marginLeft', newMarginLeft);
			}
		}

		
	};
});